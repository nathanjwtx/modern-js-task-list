/* Define UI variables */
const form = document.querySelector('#task-form');
const taskList = document.querySelector('.collection');
const clearBtn = document.querySelector('.clear-tasks');
const filter = document.querySelector('#filter');
const taskInput = document.querySelector('#task');

loadEventListeners();

function loadEventListeners() {
    document.addEventListener('DOMContentLoaded', getTasks);

    form.addEventListener('submit', addTask)
    // Remove task event
    taskList.addEventListener('click', removeTask);
    // Remove all tasks
    clearBtn.addEventListener('click', removeAllTasks);
    // Filter tasks
    filter.addEventListener('keyup', filterTasks);
}

function setTasks() {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    return tasks;
}

function generateList(task) {
    const li = document.createElement('li');
    li.className = 'collection-item';
    li.appendChild(document.createTextNode(task));
    const link = document.createElement('a');
    link.className = 'delete-item secondary-content';
    link.innerHTML = '<i class="fa fa-remove"></i>';
    li.appendChild(link);

    taskList.appendChild(li);
}

function getTasks() {

    let tasks = setTasks();

    console.log(tasks.length);

    tasks.forEach(task => {
        generateList(task);
    });
}

function addTask(e) {
    if (taskInput.value === '') {
        alert('Add a task');
        return;
    }
    generateList(taskInput.value);

    storeTask(taskInput.value);

    taskInput.value = '';

    e.preventDefault();
}

// Store task in local storage
function storeTask(task) {
    let tasks = setTasks();

    tasks.push(task);

    localStorage.setItem('tasks', JSON.stringify(tasks));
}

// Remove Task
function removeTask(e) {
    if (e.target.parentElement.classList.contains('delete-item')) {
        if (confirm('Are you sure?')) {
            e.target.parentElement.parentElement.remove();

            removeTaskFromStorage(e.target.parentElement.parentElement);
        }
    }
}

function removeTaskFromStorage(taskItem) {
    let tasks = setTasks();

    tasks.forEach((task, index) => {
        if (taskItem.textContent === task) {
            tasks.splice(index, 1);
        }
    });
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

// Remove Tasks
function removeAllTasks(e) {
    while (taskList.firstChild) {
        taskList.removeChild(taskList.firstChild);
    }
    // remove a specific item from storage
    localStorage.removeItem('tasks');
    // remove all items from storage
    // localStorage.clear();
}

// Filter tasks
function filterTasks(e) {
    const text = e.target.value.toLowerCase();
    document.querySelectorAll('.collection-item').forEach( function(task) {
        const item = task.firstChild.textContent;
        if (item.toLowerCase().indexOf(text) != -1) {
            task.style.display = 'block';
        } else {
            task.style.display = 'none';
        }
    });
}